<?

class EventController
{
    // не нужен i, так как регистра небудет
    const EVENT_ID = "/^\d*$/i";

    // значение строки в кавычках
    const TABLE_NAME = event_items;

    private Event $event;

    public function eventConfirmation()
    {

        $this->event = $item = Event::findById($_GET['item_id']);

        // $GLOBALS
        $members = $_GLOBALS['members'];

        $members->addItem($item);

        if ($members->countGuestItems() > 3) {
            $members->errorMessages->add("Слишком много участников
мероприятия.");
        }

        $total = 0;
        // использовать метод countGuestItems, заместо цикла
        foreach ($members->guests as $item) {
            $total += $item;
        }
        //нет запуска сессии
        $_SESSION['total_guests'] = $total;
        // попущен коннект первым параметром
        // ошибка в запросе 'INSERT'
        // не безопасная передача через гет в запрос
        mysqli_query("INZERT INTO " . self::TABLE_NAME . " (event_id,
event_title, event_guests) VALUES (" . $this->event->event_id . ", " .
            $_GET[event_title] . ", 3)");

        // нет смысла делать статичный метод, и передавать в него экземпляр, сразу $members->send
        Members::sendEmail($members);

        // нет конкатинации и убрать скобку, либо убрать ковычки
        print "<h1>Событие: "{
        $item->title}" подтверждено!</h1>";
}

// не должно быть параметра
    public function __destruct($event)
    {
        // не реализована переменная
        $this->event_id = null;
    }
}
class Event{

    public int $event_id;

    private array $items;

    public function __construct(int $id)
    {
        $this->event_id = $id;
    }

    public static function findById(int $id): Event{
        return new self($id);
    }

    public function getItems(){
        return $this->items;
    }
}
class Members{

    private array $events = [];
    public Messages $errorMessages;

    public function __construct()
    {
        $this->errorMessages = new ErrorMessages();
    }

    public function addItem(Event $event) : void{
        $this->events[] = $event;
    }

    public static function sendEmail(Members $members){
        $members->send();
    }

    public function send(){
        mail('...','...',....);
    }

    public function countGuestItems():int{
        $output = 0;
        foreach ($this->events as $event){
            $output += count($event->getItems());
        }
        return $output;
    }
}

class ErrorMessages implements Messages {

    public function add(string $message) : void {
        // TODO: Implement add() method.
    }
}

interface Messages{
    public function add(string $message) : void;
}