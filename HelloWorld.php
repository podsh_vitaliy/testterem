<?php


namespace PodshVitaly;


class HelloWorld
{
    private string $name;
    private int $age;

    public function setName(string $name) : self{
        $this->name = $name;
        return $this;
    }

    public function setAge(int $age) : self{
        if($age < 0){
            die( '$age не может быть меньше нуля'); // либо abs
        }
        $this->age = $age;
        return $this;
    }

    public function getName() : string{
        return $this->name;
    }

    public function getAge(): int{
        return $this->age;
    }

    public function getPlural() : string{
        $word = ['год', 'года', 'лет'];
        $key = [2,0,1,1,1,2];
        return $word[($this->age%100>4&&$this->age%100<20)?2:$key[($this->age%10<5)?$this->age%10:5]];
    }
}

$entity = new HelloWorld();

$entity->setName('Vitaly')->setAge('32');

echo  "Привет, меня зовут {$entity->getName()} и мне
{$entity->getAge()} {$entity->getPlural()}.";